## za.co.iriesoft.ng-formly

Project is for learning angular-formly, especially how its going to help us solve the crazy amount of markup in ASP.net Web Forms.


### Resources

- https://scotch.io/tutorials/easy-angularjs-forms-with-angular-formly
- http://jsbin.com/gimipu/14/edit?html
- https://github.com/github/gitignore

### todo

- integrate angular messages
- see if you can style it as jquery messages


