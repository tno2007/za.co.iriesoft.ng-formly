// scripts/MainController.js

(function () {

    "use strict";

    angular
        .module("App")
        .controller("AppController", AppController);

    function AppController(province, calendar) {

        var vm = this;

        vm.model = {};

        vm.options = [
            {
                "name": "Login",
                "value": "1"
            },
            {
                "name": "Home",
                "value": "2"
            },
            {
                "name": "Work",
                "value": "3"
            }
        ];

        vm.onChange = function () {
            console.log(1);
        }

        vm.formData = {
            CorporateInformation: {
                LoginEmailAddress: "gemma.cloete@gmail.com",
                CountryOfResidence: "manitoba",
                Title: "Mr",
                Gender: "1",
                FirstName: "Lyall",
                MiddleName: "Matthew",
                LastName: "van der Linde",
                PreferredName: "Lylo",
                DateOfBirth: {
                    Day: "1",
                    Month: "2",
                    Year: "2016"
                },
                Nationality: "northwest_territories",
                PostCode: 7708,
                AddressLine1: "17 Honolulu, The Islands",
                AddressLine2: "De Tyger",
                AddressLine3: "Parow",
                PostTown: "Cape Town",
                MobileNumber: "0793874802",
                EmailPreference: ["1", "2"]
            },
            EmailPreference: ["1", "2"]
        };

        vm.mandatoryFields = [
            {
                key: "CorporateInformation.CountryOfResidence",
                type: "select",
                templateOptions: {
                    label: "Country of Residence",
                    options: province.getProvinces(),
                    required: true,
                    tooltip: true,
                    tooltipText: "The currency of the recipient account"
                },
            },
            {
                key: "CorporateInformation.FirstName",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "First Name",
                    placeholder: "Enter your middle name",
                    required: true,
                    tooltip: true,
                    readonly: true,
                    tooltipText: "The currency of the recipient account"
                }
            },
            {
                key: "CorporateInformation.MiddleName",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Middle Name",
                    placeholder: "Enter your middle name",
                    required: true
                }
            },
            {
                key: "CorporateInformation.LastName",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Last Name",
                    placeholder: "Enter your middle name",
                    required: true
                }
            },
            {
                key: "CorporateInformation.PreferredName",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Preferred Name",
                    placeholder: "Enter your middle name",
                    required: true
                }
            },
            {
                key: "CorporateInformation",
                wrapper: "dobWrapper",
                templateOptions: {
                     label: "Date of Birth (dd/mm/yyyy)",
                     tooltip: true,
                     tooltipText: "The currency of the recipient account"
                },
                fieldGroup:
                [
                    {
                        key: "DateOfBirth.Day",
                        type: "dobSelect",
                        templateOptions: {
                            options: calendar.days(),
                            className: "selectDay"
                        }
                    },
                    {
                        key: "DateOfBirth.Month",
                        type: "dobSelect",
                        templateOptions: {
                            options: calendar.months(),
                            className: "selectMonth"
                        }
                    },
                    {
                        key: "DateOfBirth.Year",
                        type: "dobSelect",
                        templateOptions: {
                            options: calendar.years(),
                            className: "selectYear"
                        }
                    }
                ]
            },
            {
                key: "CorporateInformation.Nationality",
                type: "select",
                templateOptions: {
                    label: "Nationality",
                    options: province.getProvinces(),
                    required: true
                },
            },
            {
                key: "CorporateInformation.PostCode",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Post Code",
                    required: true
                },
            },
            {
                key: "CorporateInformation.AddressLine1",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Address Line 1",
                    required: true
                },
            },
            {
                key: "CorporateInformation.AddressLine2",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Address Line 2",
                    required: true
                },
            },
            {
                key: "CorporateInformation.AddressLine3",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Address Line 3",
                    required: true
                },
            },
            {
                key: "CorporateInformation.PostTown",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Post Town",
                    required: true
                },
            },
            {
                key: "CorporateInformation.MobileNumber",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Mobile Number",
                    required: true
                },
            }
        ];

        vm.optionalFields = [
            {
                key: "CorporateInformation.Title",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Title",
                    required: true
                },
            },
            {
                key: "CorporateInformation.Gender",
                type: "radio",
                templateOptions: {
                    label: "Gender",
                    required: true,
                    options: [
                        {
                            name: "Male",
                            value: "1",
                        },
                        {
                            name: "Female",
                            value: "2"
                        }
                    ],
                    tooltip: true,
                    tooltipText: "Your gender"
                }
            },
            {
                key: "CorporateInformation.LoginEmailAddress",
                type: "inputDisabled",
                templateOptions: {
                    label: "Login Email Address"
                }
            },
            {
                key: "EmailPreference",
                type: "multiCheckbox",
                templateOptions: {
                    label: "Email Preference",
                    required: true,
                    options: vm.options,
                    tooltip: true,
                    tooltipText: "Your gender"
                }
            },
            {
                key: "CorporateInformation",
                templateOptions: {
                     label: "Email Preference",
                     tooltip: true,
                     tooltipText: "The currency of the recipient account"
                },
                fieldGroup:
                [
                    {
                        key: "EmailPreference",
                        type: "multiCheckbox",
                        templateOptions: {
                            options: vm.options
                        }
                    }
                ]
            },
        ];

    }

    function validateDriversLicense(value) {
        return /[A-Za-z]\d{4}[\s|\-]*\d{5}[\s|\-]*\d{5}$/.test(value);
    }

})();