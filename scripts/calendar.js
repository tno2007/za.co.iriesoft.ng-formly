// scripts/province.js
(function(){
    
        'use strict';
    
        angular
            .module('App')
            .factory('calendar', calendar);
    
            function calendar() {
                function days() {
                    return [
                        { "name": "1", "value":"1" },
                        { "name": "2", "value":"2" },
                        { "name": "3", "value":"3" }
                    ];
                }
                function months() {
                    return [
                        { "name": "Jan", "value":"1" },
                        { "name": "Feb", "value":"2" },
                        { "name": "Mar", "value":"3" }
                    ];
                }
                function years() {
                    return [
                        { "name": "2017", "value":"2017" },
                        { "name": "2016", "value":"2016" },
                        { "name": "2015", "value":"2015" }
                    ];
                }
                return {
                    days: days,
                    months: months,
                    years: years
                }
            }
    
    })();